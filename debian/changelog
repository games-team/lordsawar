lordsawar (0.3.2+frogknows-1.1) unstable; urgency=medium

  * Team Upload.
  * Apply Multi-Arch: hint.

  [ Patrice Duroux ]
  * Fix FTBFS with GCC-14 (Closes: #1075234)
  * Change 2 http to 2 https
  * Replace lintian complaining gnome-common by intltool,
  * Bump Standard-Versions from 4.5.0 to 4.7.0
  * Add R3: no.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 01 Dec 2024 19:43:41 +0100

lordsawar (0.3.2+frogknows-1) unstable; urgency=medium

  * New upstream version 0.3.2+frogknows.
    - This one is really the intended 0.3.2 release by upstream. The other one
      was an unmarked beta version.

 -- Markus Koschany <apo@debian.org>  Sat, 25 Apr 2020 22:42:29 +0200

lordsawar (0.3.2-1) unstable; urgency=medium

  * New upstream version 0.3.2.
  * Enable the sound again. Build-depend on libgstreamermm-1.0-dev.
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.
  * Use canonical VCS URI.
  * Drop all patches. Fixed upstream.

 -- Markus Koschany <apo@debian.org>  Sun, 19 Apr 2020 20:56:47 +0200

lordsawar (0.3.1-4) unstable; urgency=medium

  * Drop libglademm-2.4-dev from Build-Depends. (Closes: #885888)
  * Declare compliance with Debian Policy 4.1.3.
  * Use dh_missing override.
  * Switch to compat level 11.

 -- Markus Koschany <apo@debian.org>  Sun, 31 Dec 2017 17:04:58 +0100

lordsawar (0.3.1-3) unstable; urgency=medium

  * Fix FTBFS with GCC 7. (Closes: #853529)
  * Declare compliance with Debian Policy 4.0.1.

 -- Markus Koschany <apo@debian.org>  Wed, 16 Aug 2017 01:39:37 +0200

lordsawar (0.3.1-2) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.0.0.
  * Drop deprecated menu file and xpm icon.
  * Use https for Format field.
  * Add bigmap-cairomm.patch and fix FTBFS with cairomm 1.12.2.
    (Closes: #866988)

 -- Markus Koschany <apo@debian.org>  Sat, 08 Jul 2017 16:55:44 +0200

lordsawar (0.3.1-1) unstable; urgency=medium

  * New upstream version 0.3.1.
  * Drop gcc6.patch. Fixed upstream.
  * Drop reproducible-build.patch. Fixed upstream.
  * Remove dh-autoreconf from Build-Depends.

 -- Markus Koschany <apo@debian.org>  Mon, 03 Oct 2016 16:19:18 +0200

lordsawar (0.3.0-4) unstable; urgency=medium

  * Add gcc6.patch and fix FTBFS with GCC-6. (Closes: #839323)
    Thanks to Lucas Nussbaum for the report.
  * Switch to compat level 10.

 -- Markus Koschany <apo@debian.org>  Sat, 01 Oct 2016 16:43:56 +0200

lordsawar (0.3.0-3) unstable; urgency=medium

  * Add reproducible-build.patch and make the build reproducible.
    Thanks to Reiner Herrmann for the report and patch and Alexandre Detiste
    for his initial work. (Closes: #828898)
  * Declare compliance with Debian Policy 3.9.8.
  * Vcs-Git: Use https.
  * debian/watch: Use version=4.

 -- Markus Koschany <apo@debian.org>  Thu, 30 Jun 2016 01:40:29 +0200

lordsawar (0.3.0-2) unstable; urgency=medium

  * Rebuild lordsawar against the latest version of libxml++2.6-2v5 and fix
    segmentation fault on startup.
  * Update my e-mail address.
  * Vcs-Browser: Use https.

 -- Markus Koschany <apo@debian.org>  Mon, 02 Nov 2015 18:14:43 +0100

lordsawar (0.3.0-1) unstable; urgency=medium

  [ Alexander Reichle-Schmehl ]
  * Remove myself from uploaders.

  [ Markus Koschany ]
  * Imported Upstream version 0.3.0.
  * Acknowledge last NMU from Matthias Klose. Thank you.
  * debian/lordsawar.menu: Add missing icon entry. (Closes: #726812)
  * Switch to source format 3.0 (quilt).
  * Update debian/copyright for new release. Switch to copyright format 1.0.
  * debian/rules:
    - Add get-orig-source target.
    - Export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed.
    - Build with autoreconf and --parallel.
    - Switch to dh sequencer and simplify debian/rules.
    - Disable sound until gstreamermm-1.0 is available.
  * Update debian/watch file for new download mirror.
  * Drop ftbfs_as-needed.diff. No longer necessary.
  * Drop README.source. Source format 3.0 uses quilt by default.
  * wrap-and-sort -sa.
  * debian/control:
    - Change priority from extra to optional.
    - Declare compliance with Debian Policy 3.9.6.
    - Add myself to Uploaders.
    - Update homepage field and point to nongnu.org/lordsawar.
    - lordsawar-data: Change Recommends: lordsawar to Suggests: lordsawar.
    - Update package description. Network play is not broken.
    - Remove the following build-dependencies:
      + quilt, libboost-dev, uuid-dev, libsdl1.2-mixer, libtar-dev,
        libgtkmm2.4-dev, intltool
    - Add the following build-dependencies:
      + libxml++2.6-dev, libarchive-dev, libgtkmm-3.0-dev, libxslt1-dev,
        gnome-common, libsigc++-2.0-dev.
  * Use compat level 9 and require debhelper >= 9.
  * Move the package to Git.
  * Drop Debian specific desktop files and man pages. Install upstream files
    instead.
  * Drop lordsawar.docs. README, NEWS and TODO files are not useful enough.
  * Remove lordsawar-upgrade-file because it is not really useful for normal
    users.
  * Add lordsawar-data.links and symlink /usr/share/games/lordsawar to
    /usr/share/lordsawar. This ensures the translations can be found correctly
    and is simpler to maintain than patching the build system.

 -- Markus Koschany <apo@gambaru.de>  Fri, 03 Jul 2015 21:00:02 +0200

lordsawar (0.2.0-2.1) unstable; urgency=low

  * Non maintainer upload.
  * Fix build failure with GCC-4.7. Closes: #674320.

 -- Matthias Klose <doko@debian.org>  Wed, 30 May 2012 12:55:45 +0000

lordsawar (0.2.0-2) unstable; urgency=low

  * Stopp build-depending on libggz-dev, libggzdmod-dev and libggzmod-dev.
    Apparently they are no longer used (Closes: #655401)
  * Include ftbfs_as-needed.diff by Ilya Barygin to fix FTBFS with ld
    --as-needed (Closes: #631738)
  * Drop debian/patches/{ftbfs_gcc-4.3_fix2.diff,ftbfs_gcc-4.3_fix.diff} as
    they are no longer used
  * Fix description-synopsis-starts-with-article from lintian by removing
    two a's
  * Add recommended targets build-arch and build-indep to debian/rules
  * Bump standards (no further changes needed)

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Thu, 19 Jan 2012 15:03:53 +0100

lordsawar (0.2.0-1) unstable; urgency=low

  * New upstream release
    * Doesn't use libgnet anymore (Closes: #572827)
  * Bump standards version to 3.9.1 (no changes needed)

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Tue, 29 Mar 2011 12:46:16 +0200

lordsawar (0.1.8-1) unstable; urgency=low

  * New upstream release.
  * Add misc:Depends for -data package.
  * Bump Standards Version to 3.8.4. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Sat, 10 Apr 2010 09:29:33 -0400

lordsawar (0.1.6-1) unstable; urgency=low

  [ Barry deFreese ]
  * New upstream release.
    + Drop libsdl-image1.2 from build-deps, no longer needed.
  * Add README.source for quilt patch system.
  * Clean up debian/copyright some.
  * Bump Standards Version to 3.8.3. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Wed, 21 Oct 2009 08:02:12 -0400

lordsawar (0.1.5-1) unstable; urgency=low

  [ Alexander Reichle-Schmehl ]
  * Adopt debian/control to my new name

  [ Barry deFreese ]
  * New upstream release.
    + Printfs dropped from network-connections.c. (Closes: #510767).

 -- Barry deFreese <bdefreese@debian.org>  Sun, 01 Mar 2009 17:43:35 -0500

lordsawar (0.1.4-1) unstable; urgency=low

  [ Barry deFreese ]
  * New upstream release.
  * Move 0.0.8-2.1 changelog entry to correct point in changelog.
  * Make lordsawar-data suggest lordsawar.
  * Update my e-mail address.
  * Add build-depends on intltool, uuid-dev, and libboost-dev.
  * Don't install locales since there are no translations currently.
  * Add simple man page for new lordsawar-pbm binary.
  * Drop gcc4.3 patches as they have been fixed upstream.

 -- Barry deFreese <bdefreese@debian.org>  Sat, 20 Dec 2008 13:52:12 -0500

lordsawar (0.1.0-1) unstable; urgency=low

  * New upstream release.
  * ftbfs_gcc-4.3_fix2.diff - Introduces some new gcc-4.3 build errors.
  * Fix watch file for new upstream URL.

 -- Barry deFreese <bddebian@comcast.net>  Tue, 17 Jun 2008 16:07:00 -0400

lordsawar (0.0.9-1) unstable; urgency=low

  [ Barry deFreese ]
  * New upstream release.
    + Fixes gcc-4.3 builds so drop ftbfs_gcc-4.3_fix.diff.
    + Add new build-dependency for libgnet-dev.
  * Add simple man page for new lordsawar-tile-editor.
  * Add desktop file for lordsawar-tile-editor.
  * Remove French translation on install.

  [ Gonéri Le Bouder ]
  * bump Debian Policy to 3.8.0. No change needed.
  * fix wording in the 0.0.8-3 entry of the Debian changelog

 -- Barry deFreese <bddebian@comcast.net>  Tue, 17 Jun 2008 11:15:26 +0200

lordsawar (0.0.8-3) unstable; urgency=low

  [ Gonéri Le Bouder ]
  * reverse my change to avoid autoreconf, the old behaviour was wanted
    (Closes: #474856) Thanks Peter De Wachter

 -- Debian Games Team <pkg-games-devel@lists.alioth.debian.org>  Sat, 26 Apr 2008 20:23:06 +0200

lordsawar (0.0.8-2.1) unstable; urgency=high

  [ Eddy Petrișor ]
  * fix Vcs-Svn URL to point to svn, not the incomplete ssh location

  [ Alexander Schmehl ]
  * Added patches/ftbfs_gcc-4.3_fix.diff to fix FTBFS with gcc 4.3
    (Closes: #474856)

  [ Gonéri Le Bouder ]
  * touch aclocal.m4, Makefile.in and configure to make autotools happy
    and avoid autoreconf with ftbfs_gcc-4.3_fix.diff
  * clean the .pc directory
  *

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sat, 26 Apr 2008 20:23:06 +0200

lordsawar (0.0.8-2) unstable; urgency=high

  [ Eddy Petrișor ]
  * fix Vcs-Svn URL to point to svn, not the incomplete ssh location

  [ Alexander Schmehl ]
  * Added patches/ftbfs_gcc-4.3_fix.diff to fix FTBFS with gcc 4.3
    (Closes: #474856)

  [ Gonéri Le Bouder ]
  * touch aclocal.m4, Makefile.in and configure to make autotools happy
    and avoid autoreconf with ftbfs_gcc-4.3_fix.diff
  * clean the .pc directory

 -- Debian Games Team <pkg-games-devel@lists.alioth.debian.org>  Sat, 26 Apr 2008 03:32:39 +0200

lordsawar (0.0.8-1) unstable; urgency=low

  [ Alexander Schmehl ]
  * New stable upstream release
  * added myself to uploaders
  * added libggz-dev, libggzdmod-dev and libggzmod-dev to build-depends;
    lordsawar can make use of them :)
    Thanks to Lucas Nussbaum and his "build daemon from hell" for detecting
    this issue
  * Adding a "Recommends: lordsawar" to the -data package

 -- Alexander Schmehl <tolimar@debian.org>  Wed, 13 Feb 2008 13:21:26 +0100

lordsawar (0.0.7-1) unstable; urgency=low

  [ Eddy Petrișor ]
  * install the desktop file in the proper place - thanks to Marco
    Rodrigues.

  [ Barry deFreese ]
  * New upstream release. (Closes: #452209, #452906, #452364, #452812).
  * Remove XS- prefix from VCS fields.
  * Bump Standards Version to 3.7.3. (No changes needed).
  * Fix install files.
  * Add call to dh_desktop.
  * Add icon for desktop file.
  * Remove references to in-game help in man pages. (Closes: #452812).

 -- Barry deFreese <bddebian@comcast.net>  Wed, 28 Nov 2007 00:06:13 +0200

lordsawar (0.0.4-1) unstable; urgency=low

  * New upstream release
  * Fix desktop files (files were reversed)
  * Add desktop file and simple manpage for new binary lordsawar-army-editor
  * Syntax fixes on manpages
  * Move manpages to correct section (6)

 -- Barry deFreese <bddebian@comcast.net>  Mon, 29 Oct 2007 15:38:06 -0400

lordsawar (0.0.3-1) unstable; urgency=low

  * Initial release
  * Add simple man pages for executables
  * Split into binary and data packages
  * Move binaries to /usr/games
  * Move shared data to /usr/share/games
  * Override upstream desktop files until they are fixed

 -- Barry deFreese <bddebian@comcast.net>  Thu, 27 Sep 2007 21:27:07 -0400
